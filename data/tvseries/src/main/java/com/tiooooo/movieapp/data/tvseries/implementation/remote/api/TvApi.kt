package com.tiooooo.movieapp.data.tvseries.implementation.remote.api

import com.tiooooo.core.network.data.PagingResponse
import com.tiooooo.movieapp.data.tvseries.implementation.remote.response.casts.CastsTvContainerResponse
import com.tiooooo.movieapp.data.tvseries.implementation.remote.response.detail.TvDetailResponse
import com.tiooooo.movieapp.data.tvseries.implementation.remote.response.genre.GenreTvListResponse
import com.tiooooo.movieapp.data.tvseries.implementation.remote.response.list.TvResultResponse
import com.tiooooo.movieapp.data.tvseries.implementation.remote.response.review.TvReviewResponse
import com.tiooooo.movieapp.data.tvseries.implementation.remote.response.video.TvVideoResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface TvApi {
    @GET("genre/tv/list")
    suspend fun getGenres(
    ): GenreTvListResponse

    @GET("tv/{type}")
    suspend fun getMovies(
        @Path("type") type: String = "popular",
        @Query("page") page: Int = 1,
    ): PagingResponse<TvResultResponse>

    @GET("discover/tv")
    suspend fun getDiscoverMovie(
        @Query("with_genre") genre: String = "28",
        @Query("page") page: Int = 1,
    ): PagingResponse<TvResultResponse>

    @GET("tv/{movieId}")
    suspend fun getDetailMovie(
        @Path("movieId") movieId: String,
    ): TvDetailResponse

    @GET("tv/{movieId}/reviews")
    suspend fun getMovieReviews(
        @Path("movieId") movieId: String,
        @Query("page") page: Int = 1,
    ): PagingResponse<TvReviewResponse>

    @GET("tv/{movieId}/credits")
    suspend fun getMovieCasts(
        @Path("movieId") movieId: String,
    ): CastsTvContainerResponse

    @GET("tv/{movieId}/videos")
    suspend fun getMovieVideo(
        @Path("movieId") movieId: String,
    ): PagingResponse<TvVideoResponse>

    @GET("search/tv")
    suspend fun getMovieByQuery(
        @Query("query") query: String,
        @Query("page") page: Int = 1,
    ): PagingResponse<TvResultResponse>
}
