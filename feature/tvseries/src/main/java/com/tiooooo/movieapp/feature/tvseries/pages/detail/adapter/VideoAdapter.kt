package com.tiooooo.movieapp.feature.tvseries.pages.detail.adapter


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.tiooooo.core.R
import com.tiooooo.core.databinding.ItemVideoBinding
import com.tiooooo.movieapp.data.tvseries.api.model.video.TvVideo

class VideoAdapter(private val listCast: List<TvVideo>) :
    RecyclerView.Adapter<VideoAdapter.VideoViewHolder>() {

    var onItemClick: ((url: String) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VideoViewHolder {
        return VideoViewHolder(
            ItemVideoBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: VideoViewHolder, position: Int) {
        holder.bindItem(listCast[position])
        holder.itemView.setOnClickListener {
            onItemClick?.invoke(listCast[position].key)
        }
    }

    override fun getItemCount(): Int = listCast.size

    class VideoViewHolder(val binding: ItemVideoBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bindItem(tvVideo: TvVideo) {
            binding.ivThumbnail.load("https://img.youtube.com/vi/${tvVideo.key}/0.jpg") {
                placeholder(R.drawable.ic_image)
                error(R.drawable.ic_image)
                crossfade(true)
            }

        }
    }

}
