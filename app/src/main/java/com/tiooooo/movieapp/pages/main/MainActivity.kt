package com.tiooooo.movieapp.pages.main

import com.tiooooo.core.R as CoreR
import android.os.Handler
import android.os.Looper
import android.widget.Toast
import com.tiooooo.core.base.BaseActivity
import com.tiooooo.movieapp.databinding.ActivityMainBinding
import com.tiooooo.movieapp.feature.movie.pages.main.MovieFragment
import com.tiooooo.movieapp.feature.tvseries.pages.favorite.TvFavoriteFragment
import com.tiooooo.movieapp.feature.tvseries.pages.main.TvSeriesFragment
import com.tiooooo.movieapp.pages.favorite.FavoriteFragment
import dagger.hilt.android.AndroidEntryPoint
import me.ertugrul.lib.OnItemSelectedListener


@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding>() {
    private var currentPosition = 0
    private var doubleBackPressedOnce = false

    override fun getViewBinding() = ActivityMainBinding.inflate(layoutInflater)

    override fun initView() {
        val viewPagerAdapter = SectionMenuViewPager(
            supportFragmentManager, lifecycle, listOf(
                MovieFragment(),
                TvSeriesFragment(),
                FavoriteFragment(),
            )
        )
        binding.viewPager.adapter = viewPagerAdapter
        binding.viewPager.isUserInputEnabled = false
        binding.viewPager.offscreenPageLimit = 5

        binding.bottomBar.setOnItemSelectListener(object : OnItemSelectedListener {
            override fun onItemSelect(pos: Int) {
                binding.apply {
                    viewPager.setCurrentItem(pos, false)
                    currentPosition = pos
                }
            }
        })
    }

    @Deprecated("Deprecated in Java")
    override fun onBackPressed() {
        if (currentPosition == 0) {
            val currentFragment = supportFragmentManager.fragments.firstOrNull()

            if (currentFragment is MovieFragment && currentFragment.handleBackPressed()) {
                if (doubleBackPressedOnce) {
                    super.onBackPressed()
                    return
                }

                handleExitApp()
            }
        } else if (currentPosition == 1) {
            val currentFragment = supportFragmentManager.fragments[1]

            if (currentFragment is TvSeriesFragment && currentFragment.handleBackPressed()) {
                if (doubleBackPressedOnce) {
                    super.onBackPressed()
                    return
                }

                handleExitApp()
            }
        } else {
            if (doubleBackPressedOnce) {
                super.onBackPressed()
                return
            }

            handleExitApp()
        }
    }

    private fun handleExitApp(){
        doubleBackPressedOnce = true
        Toast.makeText(
            this,
            getString(CoreR.string.text_press_once_again_to_close_app), Toast.LENGTH_SHORT
        ).show()
        Handler(Looper.getMainLooper()).postDelayed({
            doubleBackPressedOnce = false
        }, 2000)
    }

}
