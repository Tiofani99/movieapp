package com.tiooooo.movieapp.pages.favorite

import android.content.Context
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.tiooooo.movieapp.R
import com.tiooooo.movieapp.feature.movie.pages.favorite.MovieFavoriteFragment
import com.tiooooo.movieapp.feature.tvseries.pages.favorite.TvFavoriteFragment

class SectionsPagerAdapter(fa: FragmentActivity) :
    FragmentStateAdapter(fa) {

    override fun getItemCount(): Int = TAB_TITLES.size

    fun getTitle(position: Int, context: Context): String =
        context.resources.getString(TAB_TITLES[position])


    override fun createFragment(position: Int): Fragment {
        return if (position == 0) {
            MovieFavoriteFragment()
        } else {
            TvFavoriteFragment()
        }
    }

    companion object {
        @StringRes
        private val TAB_TITLES = intArrayOf(R.string.movie_tab, R.string.tv_series_tab)
    }
}
