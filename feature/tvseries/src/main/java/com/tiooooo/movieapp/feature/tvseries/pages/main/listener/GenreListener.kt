package com.tiooooo.movieapp.feature.tvseries.pages.main.listener

import com.tiooooo.movieapp.data.tvseries.api.model.list.GenreTv

interface GenreListener {
    fun onClick(list: ArrayList<GenreTv>, position: Int)
}
