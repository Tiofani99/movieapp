package com.tiooooo.movieapp.feature.tvseries.pages.main

import android.content.Intent
import android.view.inputmethod.EditorInfo
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import com.tiooooo.core.base.BaseFragment
import com.tiooooo.core.extensions.collectFlow
import com.tiooooo.core.network.data.States
import com.tiooooo.core.network.data.handleStates
import com.tiooooo.movieapp.data.tvseries.api.model.list.GenreTv
import com.tiooooo.movieapp.data.tvseries.implementation.local.entity.SearchTvHistoryEntity
import com.tiooooo.movieapp.feature.tvseries.R
import com.tiooooo.movieapp.feature.tvseries.databinding.FragmentTvSeriesBinding
import com.tiooooo.movieapp.feature.tvseries.pages.detail.DetailTvActivity
import com.tiooooo.movieapp.feature.tvseries.pages.genre.GenreTvActivity
import com.tiooooo.movieapp.feature.tvseries.pages.list.ListTvActivity
import com.tiooooo.movieapp.feature.tvseries.pages.main.adapter.SearchHistoryAdapter
import com.tiooooo.movieapp.feature.tvseries.pages.main.adapter.genre.GenreContainerAdapter
import com.tiooooo.movieapp.feature.tvseries.pages.main.adapter.poster.PosterContainerAdapter
import com.tiooooo.movieapp.feature.tvseries.pages.main.listener.GenreListener
import com.tiooooo.movieapp.feature.tvseries.pages.main.listener.PosterListener
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TvSeriesFragment : BaseFragment<FragmentTvSeriesBinding>(R.layout.fragment_tv_series) {
    private val tvViewModel: TvViewModel by viewModels()

    // parent adapter
    private lateinit var concatAdapter: ConcatAdapter

    private val genreContainerAdapter = GenreContainerAdapter(handleGenreListener())
    private val nowPlayingContainerAdapter =
        PosterContainerAdapter("Airing Today", "airing_today", handlePosterListener())
    private val popularContainerAdapter =
        PosterContainerAdapter("Popular", "popular", handlePosterListener())
    private val topRatedContainerAdapter = PosterContainerAdapter(
        "Top Rated",
        "top_rated",
        handlePosterListener()
    )
    private val upComingContainerAdapter = PosterContainerAdapter(
        "On The Air",
        "on_the_air",
        handlePosterListener()
    )

    private val searchHistoryAdapter = SearchHistoryAdapter()
    private var currentSearchHistory: List<SearchTvHistoryEntity> = listOf()

    override fun initView() {
        super.initView()
        setupAdapter()
        tvViewModel.getGenres()
        tvViewModel.getNowPlaying()
        tvViewModel.getPopular()
        tvViewModel.getTopRated()
        tvViewModel.getUpComing()
        binding.rvSearchHistory.apply {
            adapter = searchHistoryAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private fun setupAdapter() {
        concatAdapter = ConcatAdapter(genreContainerAdapter)
        concatAdapter.addAdapter(nowPlayingContainerAdapter)
        concatAdapter.addAdapter(popularContainerAdapter)
        concatAdapter.addAdapter(topRatedContainerAdapter)
        concatAdapter.addAdapter(upComingContainerAdapter)

        binding.rvContainer.adapter = concatAdapter
        binding.rvContainer.layoutManager = LinearLayoutManager(requireContext())
    }

    override fun setSubscribeToLiveData() {
        collectFlow(tvViewModel.genres) { state ->
            genreContainerAdapter.setLoading(state is States.Loading)
            state.handleStates(
                loadingBlock = {},
                successBlock = { genreContainerAdapter.setData(it.genreList) },
                emptyBlock = { concatAdapter.removeAdapter(genreContainerAdapter) },
                errorBlock = { concatAdapter.removeAdapter(genreContainerAdapter) }
            )
            checkAdapter()
        }

        collectFlow(tvViewModel.nowPlaying) { state ->
            nowPlayingContainerAdapter.setLoading(state is States.Loading)
            state.handleStates(
                loadingBlock = {},
                successBlock = { nowPlayingContainerAdapter.setData(it) },
                emptyBlock = { concatAdapter.removeAdapter(nowPlayingContainerAdapter) },
                errorBlock = { concatAdapter.removeAdapter(nowPlayingContainerAdapter) }
            )
            checkAdapter()
        }

        collectFlow(tvViewModel.popular) { state ->
            popularContainerAdapter.setLoading(state is States.Loading)
            state.handleStates(
                loadingBlock = {},
                successBlock = { popularContainerAdapter.setData(it) },
                emptyBlock = { concatAdapter.removeAdapter(popularContainerAdapter) },
                errorBlock = { concatAdapter.removeAdapter(popularContainerAdapter) }
            )
            checkAdapter()
        }

        collectFlow(tvViewModel.topRated) { state ->
            topRatedContainerAdapter.setLoading(state is States.Loading)
            state.handleStates(
                loadingBlock = {},
                successBlock = { topRatedContainerAdapter.setData(it) },
                emptyBlock = { concatAdapter.removeAdapter(topRatedContainerAdapter) },
                errorBlock = { concatAdapter.removeAdapter(topRatedContainerAdapter) }
            )
            checkAdapter()
        }

        collectFlow(tvViewModel.upComing) { state ->
            upComingContainerAdapter.setLoading(state is States.Loading)
            state.handleStates(
                loadingBlock = {},
                successBlock = { upComingContainerAdapter.setData(it) },
                emptyBlock = { concatAdapter.removeAdapter(upComingContainerAdapter) },
                errorBlock = { concatAdapter.removeAdapter(upComingContainerAdapter) }
            )
            checkAdapter()
        }

        tvViewModel.getSearchHistory().observe(this) {
            currentSearchHistory = it
            searchHistoryAdapter.setData(it)
        }
    }

    override fun initListener() {
        binding.apply {
            searchView.editText.setOnEditorActionListener { v, actionId, event ->
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    searchBar.text = searchView.text
                    searchView.hide()
                    val query = searchView.text.toString()
                    if (query.isNotEmpty()) {
                        val searchHistory = SearchTvHistoryEntity(
                            id = System.currentTimeMillis(),
                            query = query,
                            lastUpdated = System.currentTimeMillis(),
                        )
                        tvViewModel.addSearchHistory(currentSearchHistory, searchHistory)
                        val intent =
                            Intent(
                                requireActivity(),
                                ListTvActivity::class.java
                            ).apply {
                                putExtra(
                                    ListTvActivity.EXTRA_TITLE,
                                    "Result for $query"
                                )
                                putExtra(
                                    ListTvActivity.EXTRA_QUERY,
                                    query
                                )
                            }
                        startActivity(intent)
                    }
                    return@setOnEditorActionListener true
                }
                return@setOnEditorActionListener false
            }

            searchHistoryAdapter.apply {
                onClicked = {
                    searchBar.text = it.query
                    searchView.hide()
                    tvViewModel.addSearchHistory(currentSearchHistory, it)
                    val intent = Intent(
                        requireActivity(),
                        ListTvActivity::class.java
                    ).apply {
                        putExtra(
                            ListTvActivity.EXTRA_TITLE,
                            "Result for ${it.query}"
                        )
                        putExtra(
                            ListTvActivity.EXTRA_QUERY,
                            it.query
                        )
                    }
                    startActivity(intent)
                }
                onDeleteClicked = {
                    tvViewModel.deleteSearchHistory(searchHistory = it)
                }
            }
        }
    }

    private fun checkAdapter() {
        binding.nestedScrollView.isVisible = concatAdapter.adapters.isNotEmpty()
        binding.layoutError.root.isVisible = concatAdapter.adapters.isEmpty()
        binding.layoutError.btnTryAgain.setOnClickListener {
            initView()
        }
    }

    private fun handlePosterListener(): PosterListener {
        return object : PosterListener {
            override fun onClickSeeAll(title: String, type: String) {
                val intent = Intent(
                    requireActivity(),
                    ListTvActivity::class.java
                ).apply {
                    putExtra(
                        ListTvActivity.EXTRA_TITLE,
                        title
                    )
                    putExtra(
                        ListTvActivity.EXTRA_TYPE,
                        type
                    )
                }
                startActivity(intent)
            }

            override fun onClickDetail(tvId: Long) {
                val intent = Intent(
                    requireActivity(),
                    DetailTvActivity::class.java
                ).apply {
                    putExtra(
                        DetailTvActivity.EXTRA_ID,
                        tvId.toString()
                    )
                }
                startActivity(intent)
            }
        }
    }

    private fun handleGenreListener(): GenreListener {
        return object : GenreListener {
            override fun onClick(list: ArrayList<GenreTv>, position: Int) {
                val intent = Intent(
                    requireActivity(),
                    GenreTvActivity::class.java
                ).apply {
                    putParcelableArrayListExtra(
                        GenreTvActivity.EXTRA_GENRE,
                        list
                    )
                    putExtra(
                        GenreTvActivity.EXTRA_POSITION,
                        position
                    )
                }
                startActivity(intent)
            }
        }
    }

    fun handleBackPressed(): Boolean {
        if (binding.searchView.isShowing) {
            binding.searchView.hide()
            return false
        }
        return true
    }

}
