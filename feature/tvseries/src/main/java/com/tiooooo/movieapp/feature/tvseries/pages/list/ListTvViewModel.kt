package com.tiooooo.movieapp.feature.tvseries.pages.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.tiooooo.movieapp.data.tvseries.api.model.list.TvResult
import com.tiooooo.movieapp.data.tvseries.api.repository.TvRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ListTvViewModel @Inject constructor(
    private val tvRepository: TvRepository,
) : ViewModel() {

    private val _tvSeries = MutableLiveData<PagingData<TvResult>>()
    val tvSeries: LiveData<PagingData<TvResult>> get() = _tvSeries

    fun getAllTvByType(
        type: String = "popular",
    ) = viewModelScope.launch {
        tvRepository.getAllTvByType(type).cachedIn(viewModelScope).collectLatest {
            _tvSeries.value = it
        }
    }

    fun getTvByQuery(
        query: String = "",
    ) = viewModelScope.launch {
        tvRepository.getTvByQuery(query).cachedIn(viewModelScope).collectLatest {
            _tvSeries.value = it
        }
    }

}
