package com.tiooooo.movieapp.data.tvseries.implementation.datasource

import com.tiooooo.core.base.BasePagingSource
import com.tiooooo.movieapp.data.tvseries.implementation.remote.api.TvApi
import com.tiooooo.movieapp.data.tvseries.api.model.list.TvResult
import com.tiooooo.movieapp.data.tvseries.implementation.remote.response.list.mapToTvResult


class DiscoverTvPagingSource(
    private val tvApi: TvApi,
    private val genreId: String,
) : BasePagingSource<Int, TvResult>(fetch = { position ->
    val res = tvApi.getDiscoverMovie(genreId, position)
    res.data?.map { it.mapToTvResult() } ?: emptyList()
})

