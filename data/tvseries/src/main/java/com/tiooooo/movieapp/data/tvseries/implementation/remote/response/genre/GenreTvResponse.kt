package com.tiooooo.movieapp.data.tvseries.implementation.remote.response.genre

import com.google.gson.annotations.SerializedName
import com.tiooooo.movieapp.data.tvseries.api.model.list.GenreTv
import com.tiooooo.movieapp.data.tvseries.api.model.list.GenreTvList

data class GenreTvResponse(
    @SerializedName("id") val id: Int?,
    @SerializedName("name") val name: String?,
)

data class GenreTvListResponse(
    @SerializedName("genres") val genres: List<GenreTvResponse>?,
)

fun GenreTvListResponse.toGenreTvList(): GenreTvList {
    val list = this.genres?.map { it.toGenre() } ?: listOf()
    return GenreTvList(
        genreList = list
    )
}


fun GenreTvResponse.toGenre() = GenreTv(
    id = this.id ?: 0,
    name = this.name.orEmpty()
)

