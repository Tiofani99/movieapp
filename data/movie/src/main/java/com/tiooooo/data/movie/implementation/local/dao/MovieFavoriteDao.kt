package com.tiooooo.data.movie.implementation.local.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.tiooooo.data.movie.implementation.local.entity.MovieFavoriteEntity

@Dao
interface MovieFavoriteDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(movieFavoriteEntity: MovieFavoriteEntity)

    @Update
    fun update(movieFavoriteEntity: MovieFavoriteEntity)

    @Delete
    fun delete(movieFavoriteEntity: MovieFavoriteEntity)

    @Query("SELECT * FROM MovieFavoriteEntity WHERE id = :id")
    fun getMovieById(id: Int): LiveData<MovieFavoriteEntity>

    @Query("SELECT * from MovieFavoriteEntity ORDER BY lastUpdated DESC")
    fun getAllFavorite(): LiveData<List<MovieFavoriteEntity>>
}
