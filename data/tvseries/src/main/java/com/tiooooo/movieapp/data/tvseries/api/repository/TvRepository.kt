package com.tiooooo.movieapp.data.tvseries.api.repository

import androidx.lifecycle.LiveData
import androidx.paging.PagingData
import com.tiooooo.core.network.data.States
import com.tiooooo.movieapp.data.tvseries.api.model.casts.CastTv
import com.tiooooo.movieapp.data.tvseries.api.model.detail.TvDetail
import com.tiooooo.movieapp.data.tvseries.api.model.list.GenreTvList
import com.tiooooo.movieapp.data.tvseries.api.model.list.TvResult
import com.tiooooo.movieapp.data.tvseries.api.model.review.TvReview
import com.tiooooo.movieapp.data.tvseries.api.model.video.TvVideo
import com.tiooooo.movieapp.data.tvseries.implementation.local.entity.SearchTvHistoryEntity
import com.tiooooo.movieapp.data.tvseries.implementation.local.entity.TvFavoriteEntity
import kotlinx.coroutines.flow.Flow

interface TvRepository {

    suspend fun getTvGenres(): Flow<States<GenreTvList>>

    suspend fun getTvByType(type: String): Flow<States<List<TvResult>>>

    suspend fun getAllTvByType(type: String): Flow<PagingData<TvResult>>

    suspend fun getDiscoverTv(genreId: String): Flow<PagingData<TvResult>>

    suspend fun getDetailTv(tvId: String): Flow<States<TvDetail>>

    suspend fun getTvReviews(tvId: String): Flow<States<List<TvReview>>>

    suspend fun getAllTvReviews(tvId: String): Flow<PagingData<TvReview>>

    suspend fun getTvCasts(tvId: String): Flow<States<List<CastTv>>>

    suspend fun getTvVideos(tvId: String): Flow<States<List<TvVideo>>>

    suspend fun getTvByQuery(query: String): Flow<PagingData<TvResult>>

    fun getSearchTvHistory(): LiveData<List<SearchTvHistoryEntity>>
    suspend fun insertSearchTvHistory(search: SearchTvHistoryEntity)
    suspend fun deleteSearchTvHistory(search: SearchTvHistoryEntity)
    suspend fun updateSearchTvHistory(search: SearchTvHistoryEntity)

    fun getAllTvFavorite(): LiveData<List<TvFavoriteEntity>>
    fun getTvFavoriteById(tvId: Int): LiveData<TvFavoriteEntity>
    suspend fun insertTvFavorite(tvFavoriteEntity: TvFavoriteEntity)
    suspend fun deleteTvFavorite(tvFavoriteEntity: TvFavoriteEntity)
    suspend fun updateTvFavorite(tvFavoriteEntity: TvFavoriteEntity)
}
