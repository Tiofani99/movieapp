package com.tiooooo.movieapp.feature.tvseries.pages.review

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.tiooooo.core.databinding.ItemReviewBinding
import com.tiooooo.movieapp.data.tvseries.api.model.review.TvReview

class ListAllReviewAdapter :
    PagingDataAdapter<TvReview, ListAllReviewAdapter.ViewHolder>(Companion) {
    companion object : DiffUtil.ItemCallback<TvReview>() {
        override fun areItemsTheSame(oldItem: TvReview, newItem: TvReview): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: TvReview, newItem: TvReview): Boolean {
            return oldItem == newItem
        }
    }

    var onItemClick: ((url: String) -> Unit)? = null

    override
    fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bindItem(it)
            holder.itemBinding.btnReadFull.setOnClickListener {
                onItemClick?.invoke(getItem(position)?.url.toString())
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemReviewBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    class ViewHolder(
        private val binding: ItemReviewBinding,
    ) : RecyclerView.ViewHolder(binding.root) {

        val itemBinding = binding
        fun bindItem(review: TvReview) {
            binding.apply {
                tvName.text = review.author
                tvReview.text = review.content
                binding.btnReadFull.isVisible = review.content.length >= 300
            }
        }
    }
}
