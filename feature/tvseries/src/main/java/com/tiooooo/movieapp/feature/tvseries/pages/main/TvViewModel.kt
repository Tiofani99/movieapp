package com.tiooooo.movieapp.feature.tvseries.pages.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tiooooo.core.network.data.States
import com.tiooooo.movieapp.data.tvseries.api.model.list.GenreTvList
import com.tiooooo.movieapp.data.tvseries.api.model.list.TvResult
import com.tiooooo.movieapp.data.tvseries.api.repository.TvRepository
import com.tiooooo.movieapp.data.tvseries.implementation.local.entity.SearchTvHistoryEntity
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TvViewModel @Inject constructor(
    private val tvRepository: TvRepository,
) : ViewModel() {

    private val _genres = MutableSharedFlow<States<GenreTvList>>()
    val genres = _genres.asSharedFlow()

    private val _nowPlaying = MutableSharedFlow<States<List<TvResult>>>()
    val nowPlaying = _nowPlaying.asSharedFlow()

    private val _popular = MutableSharedFlow<States<List<TvResult>>>()
    val popular = _popular.asSharedFlow()

    private val _topRated = MutableSharedFlow<States<List<TvResult>>>()
    val topRated = _topRated.asSharedFlow()

    private val _upComing = MutableSharedFlow<States<List<TvResult>>>()
    val upComing = _upComing.asSharedFlow()

    fun getGenres() {
        viewModelScope.launch {
            _genres.emitAll(tvRepository.getTvGenres())
        }
    }

    fun getNowPlaying(type: String = "airing_today") {
        viewModelScope.launch {
            _nowPlaying.emitAll(tvRepository.getTvByType(type))
        }
    }

    fun getPopular(type: String = "popular") {
        viewModelScope.launch {
            _popular.emitAll(tvRepository.getTvByType(type))
        }
    }

    fun getTopRated(type: String = "top_rated") {
        viewModelScope.launch {
            _topRated.emitAll(tvRepository.getTvByType(type))
        }
    }

    fun getUpComing(type: String = "on_the_air") {
        viewModelScope.launch {
            _upComing.emitAll(tvRepository.getTvByType(type))
        }
    }

    //search history
    fun getSearchHistory(): LiveData<List<SearchTvHistoryEntity>> =
        tvRepository.getSearchTvHistory()

    fun addSearchHistory(
        currentList: List<SearchTvHistoryEntity>,
        searchHistory: SearchTvHistoryEntity,
    ) {
        viewModelScope.launch {
            val product = currentList.find { it.query == searchHistory.query }
            product?.let {
                tvRepository.updateSearchTvHistory(product.copy(lastUpdated = System.currentTimeMillis()))
            } ?: kotlin.run {
                tvRepository.insertSearchTvHistory(searchHistory)
            }
        }
    }

    fun deleteSearchHistory(searchHistory: SearchTvHistoryEntity) {
        viewModelScope.launch {
            tvRepository.deleteSearchTvHistory(searchHistory)
        }
    }
}
