package com.tiooooo.movieapp.feature.movie.pages.genre.adapter

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.tiooooo.data.movie.api.model.list.Genre
import com.tiooooo.movieapp.feature.movie.pages.genre.GenreFragment

class SectionGenreAdapter(
    activity: AppCompatActivity,
    private val listCategory: ArrayList<Genre>,
) : FragmentStateAdapter(activity) {

    override fun getItemCount(): Int = listCategory.size

    override fun createFragment(position: Int): Fragment {
        return GenreFragment.newInstance(listCategory, position)
    }

}
