package com.tiooooo.movieapp.data.tvseries.implementation.local.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.tiooooo.movieapp.data.tvseries.implementation.local.entity.TvFavoriteEntity

@Dao
interface TvFavoriteDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(tvFavoriteEntity: TvFavoriteEntity)

    @Update
    fun update(tvFavoriteEntity: TvFavoriteEntity)

    @Delete
    fun delete(tvFavoriteEntity: TvFavoriteEntity)

    @Query("SELECT * FROM TvFavoriteEntity WHERE id = :id")
    fun getTvSeriesById(id: Int): LiveData<TvFavoriteEntity>

    @Query("SELECT * from TvFavoriteEntity ORDER BY lastUpdated DESC")
    fun getAllFavorite(): LiveData<List<TvFavoriteEntity>>
}
