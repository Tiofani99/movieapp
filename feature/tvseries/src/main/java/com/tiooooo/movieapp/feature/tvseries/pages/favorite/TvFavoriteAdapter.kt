package com.tiooooo.movieapp.feature.tvseries.pages.favorite

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter
import coil.load
import com.tiooooo.core.R
import com.tiooooo.core.databinding.ItemPosterFullBinding
import com.tiooooo.movieapp.data.tvseries.implementation.local.entity.TvFavoriteEntity

class TvFavoriteAdapter : Adapter<TvFavoriteAdapter.ViewHolder>() {
    private val list: MutableList<TvFavoriteEntity> = mutableListOf()
    var onItemClick: ((tvId: String) -> Unit)? = null

    @SuppressLint("NotifyDataSetChanged")
    fun setData(list: List<TvFavoriteEntity>) {
        this.list.clear()
        this.list.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemPosterFullBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(list[position])
        holder.itemView.setOnClickListener {
            onItemClick?.invoke(list[position].id.toString())
        }
    }

    class ViewHolder(
        private val binding: ItemPosterFullBinding,
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bindItem(tvResult: TvFavoriteEntity) {
            binding.apply {
                ivPoster.load(tvResult.imageUrl) {
                    placeholder(R.drawable.ic_image)
                    error(R.drawable.ic_image)
                    crossfade(true)
                }
                tvTitle.text = tvResult.title
            }
        }
    }
}
