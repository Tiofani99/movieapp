package com.tiooooo.movieapp.data.tvseries.api.model.list

import com.tiooooo.movieapp.data.tvseries.api.model.list.GenreTv

data class GenreTvList(
    val genreList: List<GenreTv>,
)

