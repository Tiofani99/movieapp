package com.tiooooo.movieapp.data.tvseries.implementation.repository

import androidx.lifecycle.LiveData
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.tiooooo.core.network.data.States
import com.tiooooo.core.network.data.toError
import com.tiooooo.movieapp.data.tvseries.api.model.casts.CastTv
import com.tiooooo.movieapp.data.tvseries.api.model.detail.TvDetail
import com.tiooooo.movieapp.data.tvseries.api.model.list.GenreTvList
import com.tiooooo.movieapp.data.tvseries.api.model.list.TvResult
import com.tiooooo.movieapp.data.tvseries.api.model.review.TvReview
import com.tiooooo.movieapp.data.tvseries.api.model.video.TvVideo
import com.tiooooo.movieapp.data.tvseries.api.repository.TvRepository
import com.tiooooo.movieapp.data.tvseries.implementation.datasource.DiscoverTvPagingSource
import com.tiooooo.movieapp.data.tvseries.implementation.datasource.TvByQueryPagingSource
import com.tiooooo.movieapp.data.tvseries.implementation.datasource.TvPagingSource
import com.tiooooo.movieapp.data.tvseries.implementation.datasource.TvReviewPagingSource
import com.tiooooo.movieapp.data.tvseries.implementation.local.dao.SearchTvHistoryDao
import com.tiooooo.movieapp.data.tvseries.implementation.local.dao.TvFavoriteDao
import com.tiooooo.movieapp.data.tvseries.implementation.local.entity.SearchTvHistoryEntity
import com.tiooooo.movieapp.data.tvseries.implementation.local.entity.TvFavoriteEntity
import com.tiooooo.movieapp.data.tvseries.implementation.remote.api.TvApi
import com.tiooooo.movieapp.data.tvseries.implementation.remote.response.casts.toTvCast
import com.tiooooo.movieapp.data.tvseries.implementation.remote.response.detail.toTvDetail
import com.tiooooo.movieapp.data.tvseries.implementation.remote.response.genre.toGenreTvList
import com.tiooooo.movieapp.data.tvseries.implementation.remote.response.list.mapToTvResult
import com.tiooooo.movieapp.data.tvseries.implementation.remote.response.review.toTvReview
import com.tiooooo.movieapp.data.tvseries.implementation.remote.response.video.toTvVideo
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.withContext

class TvRepositoryImpl(
    private val tvApi: TvApi,
    private val searchTvHistoryDao: SearchTvHistoryDao,
    private val tvFavoriteDao: TvFavoriteDao,
    private val ioDispatcher: CoroutineDispatcher,
) : TvRepository {

    override suspend fun getTvGenres(): Flow<States<GenreTvList>> {
        return flow {
            try {
                emit(States.Loading)
                val response = tvApi.getGenres()
                emit(States.Success(data = response.toGenreTvList()))
            } catch (e: Exception) {
                emit(e.toError())
            }
        }.flowOn(ioDispatcher)
    }

    override suspend fun getTvByType(type: String): Flow<States<List<TvResult>>> {
        return flow {
            try {
                emit(States.Loading)
                val response = tvApi.getMovies(type)
                response.data?.let { list ->
                    emit(States.Success(data = list.map { it.mapToTvResult() }))
                } ?: run {
                    emit(States.Empty)
                }
            } catch (e: Exception) {
                emit(e.toError())
            }
        }.flowOn(ioDispatcher)
    }

    override suspend fun getAllTvByType(type: String): Flow<PagingData<TvResult>> {
        return Pager(
            config = PagingConfig(
                pageSize = 20
            ),
            pagingSourceFactory = {
                TvPagingSource(tvApi, type)
            }
        ).flow
    }

    override suspend fun getDiscoverTv(genreId: String): Flow<PagingData<TvResult>> {
        return Pager(
            config = PagingConfig(
                pageSize = 20
            ),
            pagingSourceFactory = {
                DiscoverTvPagingSource(tvApi, genreId)
            }
        ).flow
    }

    override suspend fun getDetailTv(tvId: String): Flow<States<TvDetail>> {
        return flow {
            try {
                emit(States.Loading)
                kotlinx.coroutines.delay(1000)
                val response = tvApi.getDetailMovie(tvId)
                emit(States.Success(data = response.toTvDetail()))
            } catch (e: Exception) {
                emit(e.toError())
            }
        }.flowOn(ioDispatcher)
    }

    override suspend fun getTvReviews(tvId: String): Flow<States<List<TvReview>>> {
        return flow {
            try {
                emit(States.Loading)
                val response = tvApi.getMovieReviews(tvId)
                response.data?.let { list ->
                    emit(States.Success(data = list.map { it.toTvReview() }))
                } ?: run {
                    emit(States.Empty)
                }
            } catch (e: Exception) {
                emit(e.toError())
            }
        }.flowOn(ioDispatcher)
    }

    override suspend fun getAllTvReviews(tvId: String): Flow<PagingData<TvReview>> {
        return Pager(
            config = PagingConfig(
                pageSize = 20
            ),
            pagingSourceFactory = {
                TvReviewPagingSource(tvApi, tvId)
            }
        ).flow
    }

    override suspend fun getTvCasts(tvId: String): Flow<States<List<CastTv>>> {
        return flow {
            try {
                emit(States.Loading)
                val response = tvApi.getMovieCasts(tvId)
                response.cast?.let { list ->
                    emit(States.Success(data = list.map { it.toTvCast() }))
                } ?: run {
                    emit(States.Empty)
                }
            } catch (e: Exception) {
                emit(e.toError())
            }
        }.flowOn(ioDispatcher)
    }

    override suspend fun getTvVideos(tvId: String): Flow<States<List<TvVideo>>> {
        return flow {
            try {
                emit(States.Loading)
                val response = tvApi.getMovieVideo(tvId)
                response.data?.let { list ->
                    emit(States.Success(data = list.map { it.toTvVideo() }))
                } ?: run {
                    emit(States.Empty)
                }
            } catch (e: Exception) {
                emit(e.toError())
            }
        }.flowOn(ioDispatcher)
    }

    override suspend fun getTvByQuery(query: String): Flow<PagingData<TvResult>> {
        return Pager(
            config = PagingConfig(
                pageSize = 20
            ),
            pagingSourceFactory = {
                TvByQueryPagingSource(tvApi, query)
            }
        ).flow
    }

    override fun getSearchTvHistory(): LiveData<List<SearchTvHistoryEntity>> {
        return searchTvHistoryDao.getSearchHistory()
    }

    override suspend fun insertSearchTvHistory(search: SearchTvHistoryEntity) {
        withContext(ioDispatcher) {
            searchTvHistoryDao.insert(search)
        }
    }

    override suspend fun deleteSearchTvHistory(search: SearchTvHistoryEntity) {
        withContext(ioDispatcher) {
            searchTvHistoryDao.delete(search)
        }
    }

    override suspend fun updateSearchTvHistory(search: SearchTvHistoryEntity) {
        withContext(ioDispatcher) {
            searchTvHistoryDao.delete(search)
        }
    }

    override fun getAllTvFavorite(): LiveData<List<TvFavoriteEntity>> {
        return tvFavoriteDao.getAllFavorite()
    }

    override fun getTvFavoriteById(tvId: Int): LiveData<TvFavoriteEntity> {
        return tvFavoriteDao.getTvSeriesById(tvId)
    }

    override suspend fun insertTvFavorite(tvFavoriteEntity: TvFavoriteEntity) {
        withContext(ioDispatcher) {
            tvFavoriteDao.insert(tvFavoriteEntity)
        }
    }

    override suspend fun deleteTvFavorite(tvFavoriteEntity: TvFavoriteEntity) {
        withContext(ioDispatcher) {
            tvFavoriteDao.delete(tvFavoriteEntity)
        }
    }

    override suspend fun updateTvFavorite(tvFavoriteEntity: TvFavoriteEntity) {
        withContext(ioDispatcher) {
            tvFavoriteDao.update(tvFavoriteEntity)
        }
    }
}
