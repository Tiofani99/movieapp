package com.tiooooo.movieapp.feature.tvseries.pages.favorite

import androidx.lifecycle.ViewModel
import com.tiooooo.movieapp.data.tvseries.api.repository.TvRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class TvFavoriteViewModel @Inject constructor(
    private val tvRepository: TvRepository,
) : ViewModel() {

    fun getTvFavorites() = tvRepository.getAllTvFavorite()
}
