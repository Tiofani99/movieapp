package com.tiooooo.movieapp.data.tvseries.implementation.remote.response.video

import com.google.gson.annotations.SerializedName
import com.tiooooo.movieapp.data.tvseries.api.model.video.TvVideo

data class TvVideoResponse(
    @SerializedName("id") val id: String?,
    @SerializedName("key") val key: String?,
    @SerializedName("site") val site: String?,
    @SerializedName("name") val name: String?,
)

fun TvVideoResponse?.toTvVideo(): TvVideo = TvVideo(
    id = this?.id.orEmpty(),
    key = this?.key.orEmpty(),
    site = this?.site.orEmpty(),
    name = this?.name.orEmpty(),
)
