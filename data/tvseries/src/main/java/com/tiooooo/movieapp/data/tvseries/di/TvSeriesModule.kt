package com.tiooooo.movieapp.data.tvseries.di

import com.tiooooo.core.common.ContextProvider
import com.tiooooo.core.di.IoDispatcher
import com.tiooooo.movieapp.data.tvseries.api.repository.TvRepository
import com.tiooooo.movieapp.data.tvseries.implementation.local.TvSeriesDb
import com.tiooooo.movieapp.data.tvseries.implementation.local.dao.SearchTvHistoryDao
import com.tiooooo.movieapp.data.tvseries.implementation.local.dao.TvFavoriteDao
import com.tiooooo.movieapp.data.tvseries.implementation.remote.api.TvApi
import com.tiooooo.movieapp.data.tvseries.implementation.repository.TvRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object TvSeriesModule {

    @Provides
    @Singleton
    fun provideTvApi(retrofit: Retrofit): TvApi {
        return retrofit.create(TvApi::class.java)
    }

    @Provides
    @Singleton
    fun provideTvRepository(
        tvApi: TvApi,
        searchTvHistoryDao: SearchTvHistoryDao,
        tvFavoriteDao: TvFavoriteDao,
        @IoDispatcher ioDispatcher: CoroutineDispatcher,
    ): TvRepository {
        return TvRepositoryImpl(
            tvApi = tvApi,
            searchTvHistoryDao = searchTvHistoryDao,
            tvFavoriteDao = tvFavoriteDao,
            ioDispatcher = ioDispatcher,
        )
    }

    @Singleton
    @Provides
    fun provideTvSeriesDb(contextProvider: ContextProvider): TvSeriesDb {
        return TvSeriesDb.getInstance(contextProvider.getContext())
    }

    @Singleton
    @Provides
    fun provideSearchTvHistoryDao(tvSeriesDb: TvSeriesDb) = tvSeriesDb.searchHistoryDao()

    @Singleton
    @Provides
    fun provideTvFavoriteDao(tvSeriesDb: TvSeriesDb) = tvSeriesDb.tvFavoriteDao()
}
