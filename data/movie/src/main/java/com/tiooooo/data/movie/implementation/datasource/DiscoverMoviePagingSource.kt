package com.tiooooo.data.movie.implementation.datasource

import com.tiooooo.core.base.BasePagingSource
import com.tiooooo.data.movie.api.model.list.MovieResult
import com.tiooooo.data.movie.implementation.remote.api.MovieApi
import com.tiooooo.data.movie.implementation.remote.response.list.mapToMovieResult


class DiscoverMoviePagingSource(
    private val movieApi: MovieApi,
    private val genreId: String,
) : BasePagingSource<Int, MovieResult>(fetch = { position ->
    val res = movieApi.getDiscoverMovie(genreId, position)
    res.data?.map { it.mapToMovieResult() } ?: emptyList()
})

