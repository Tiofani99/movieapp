package com.tiooooo.movieapp.data.tvseries.api.model.video

data class TvVideo(
    val id: String,
    val key: String,
    val site: String,
    val name: String,
)
