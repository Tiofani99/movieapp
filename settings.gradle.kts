pluginManagement {
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
        maven {
            url = uri("https://www.jitpack.io" )
        }
    }
}

rootProject.name = "movieapp"

include(":app")
include(":core")
include(":data:movie")
include(":data:tvseries")
include(":feature:movie")
include(":feature:tvseries")
