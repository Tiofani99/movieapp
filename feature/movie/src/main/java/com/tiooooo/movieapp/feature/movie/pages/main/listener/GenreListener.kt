package com.tiooooo.movieapp.feature.movie.pages.main.listener

import com.tiooooo.data.movie.api.model.list.Genre

interface GenreListener {
    fun onClick(list: ArrayList<Genre>, position: Int)
}
