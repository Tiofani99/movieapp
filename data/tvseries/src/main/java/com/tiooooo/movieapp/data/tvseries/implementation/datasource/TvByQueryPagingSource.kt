package com.tiooooo.movieapp.data.tvseries.implementation.datasource

import com.tiooooo.core.base.BasePagingSource
import com.tiooooo.movieapp.data.tvseries.implementation.remote.api.TvApi
import com.tiooooo.movieapp.data.tvseries.api.model.list.TvResult
import com.tiooooo.movieapp.data.tvseries.implementation.remote.response.list.mapToTvResult

class TvByQueryPagingSource(
    private val tvApi: TvApi,
    private val query: String,
) : BasePagingSource<Int, TvResult>(
    fetch = { position ->
        val res = tvApi.getMovieByQuery(query, position)
        res.data?.map { it.mapToTvResult() } ?: emptyList()
    }
)

