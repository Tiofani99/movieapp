package com.tiooooo.movieapp.data.tvseries.implementation.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class TvFavoriteEntity(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") val id: Long = 0,
    @ColumnInfo("title") val title: String,
    @ColumnInfo("imageUrl") val imageUrl: String,
    @ColumnInfo("lastUpdated") val lastUpdated: Long? = System.currentTimeMillis(),
)
