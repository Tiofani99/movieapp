package com.tiooooo.movieapp.pages.favorite

import com.google.android.material.tabs.TabLayoutMediator
import com.tiooooo.core.base.BaseFragment
import com.tiooooo.movieapp.R
import com.tiooooo.movieapp.databinding.FragmentFavoriteBinding


class FavoriteFragment : BaseFragment<FragmentFavoriteBinding>(R.layout.fragment_favorite) {
    override fun initView() {
        super.initView()

        val sectionsPagerAdapter = SectionsPagerAdapter(requireActivity())
        binding.viewPager.adapter = sectionsPagerAdapter
        TabLayoutMediator(binding.tabs, binding.viewPager) { tab, position ->
            tab.text = sectionsPagerAdapter.getTitle(position, requireContext())
        }.attach()
    }

    override fun onDestroyView() {
        binding.viewPager.adapter = null
        super.onDestroyView()
    }
}

