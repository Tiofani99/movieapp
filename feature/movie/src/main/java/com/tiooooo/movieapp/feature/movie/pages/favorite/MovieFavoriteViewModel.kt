package com.tiooooo.movieapp.feature.movie.pages.favorite

import androidx.lifecycle.ViewModel
import com.tiooooo.data.movie.api.repository.MovieRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MovieFavoriteViewModel @Inject constructor(
    private val movieRepository: MovieRepository,
) : ViewModel() {

    fun getMovieFavorite() = movieRepository.getAllMovieFavorite()
}
