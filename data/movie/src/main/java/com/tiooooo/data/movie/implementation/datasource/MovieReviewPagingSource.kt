package com.tiooooo.data.movie.implementation.datasource

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.tiooooo.core.base.BasePagingSource
import com.tiooooo.data.movie.api.model.list.MovieResult
import com.tiooooo.data.movie.api.model.review.MovieReview
import com.tiooooo.data.movie.implementation.remote.api.MovieApi
import com.tiooooo.data.movie.implementation.remote.response.list.mapToMovieResult
import com.tiooooo.data.movie.implementation.remote.response.review.toMovieReview


class MovieReviewPagingSource(
    private val movieApi: MovieApi,
    private val movieId: String,
) : BasePagingSource<Int, MovieReview>(
    fetch = { position ->
        val res = movieApi.getMovieReviews(movieId, position)
        res.data?.map { it.toMovieReview() } ?: emptyList()
    }
)
