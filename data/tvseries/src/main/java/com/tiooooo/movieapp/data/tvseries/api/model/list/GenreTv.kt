package com.tiooooo.movieapp.data.tvseries.api.model.list

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class GenreTv(
    val id: Int,
    val name: String,
) : Parcelable

