package com.tiooooo.movieapp.feature.tvseries.pages.favorite

import android.content.Intent
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.tiooooo.core.base.BaseFragment
import com.tiooooo.core.extensions.gone
import com.tiooooo.movieapp.feature.tvseries.R
import com.tiooooo.movieapp.feature.tvseries.databinding.FragmentTvFavoriteBinding
import com.tiooooo.movieapp.feature.tvseries.pages.detail.DetailTvActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TvFavoriteFragment : BaseFragment<FragmentTvFavoriteBinding>(R.layout.fragment_tv_favorite) {

    private val tvFavoriteViewModel: TvFavoriteViewModel by viewModels()
    private val tvFavoriteAdapter = TvFavoriteAdapter()

    override fun initView() {
        binding.apply {
            rvTvSeries.adapter = tvFavoriteAdapter
            rvTvSeries.layoutManager = GridLayoutManager(requireContext(), 2)
        }
    }

    override fun initListener() {
        binding.swipeRefresh.setOnRefreshListener {
            binding.swipeRefresh.isRefreshing = false
        }

        tvFavoriteAdapter.apply {
            onItemClick = {
                val intent = Intent(requireContext(), DetailTvActivity::class.java).apply {
                    putExtra(DetailTvActivity.EXTRA_ID, it)
                }
                startActivity(intent)
            }
        }
    }

    override fun setSubscribeToLiveData() {
        tvFavoriteViewModel.getTvFavorites().observe(viewLifecycleOwner) {
            tvFavoriteAdapter.setData(it)
            binding.layoutError.apply {
                root.isVisible = it.isEmpty()
                btnTryAgain.isVisible = false
            }
            binding.layoutError.tvInfo.text = "You do not have a favorite yet"
            binding.layoutError.btnTryAgain.gone()
            binding.rvTvSeries.isVisible = it.isNotEmpty()
        }
    }
}
