package com.tiooooo.movieapp.feature.tvseries.pages.genre

import android.content.Intent
import android.os.Bundle
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import com.tiooooo.core.base.BaseFragment
import com.tiooooo.core.extensions.getLaunch
import com.tiooooo.movieapp.data.tvseries.api.model.list.GenreTv
import com.tiooooo.movieapp.feature.tvseries.R
import com.tiooooo.movieapp.feature.tvseries.databinding.FragmentGenreTvBinding
import com.tiooooo.movieapp.feature.tvseries.pages.detail.DetailTvActivity
import com.tiooooo.movieapp.feature.tvseries.pages.list.ListTvAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest

private const val EXTRA_GENRE = "EXTRA GENRE"
private const val EXTRA_POSITION = "EXTRA POSITION"

@AndroidEntryPoint
class GenreTvFragment : BaseFragment<FragmentGenreTvBinding>(R.layout.fragment_genre_tv) {
    companion object {
        @JvmStatic
        fun newInstance(genres: ArrayList<GenreTv>? = null, position: Int) =
            GenreTvFragment().apply {
                arguments = Bundle().apply {
                    putParcelableArrayList(EXTRA_GENRE, genres)
                    putInt(EXTRA_POSITION, position)
                }
            }
    }

    private var listCategory: List<GenreTv>? = listOf()
    private var position: Int = 0
    private val genreViewModel: GenreTvViewModel by viewModels()
    private lateinit var listTvAdapter: ListTvAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            listCategory = it.getParcelableArrayList(EXTRA_GENRE)
            position = it.getInt(EXTRA_POSITION, 0)
        }
    }

    override fun initView() {
        listCategory?.let {
            val categoryId = it[position].id.toString()
            genreViewModel.getDiscoverTv(categoryId)

            listTvAdapter = ListTvAdapter().apply {
                onItemClick = {
                    val intent = Intent(requireActivity(), DetailTvActivity::class.java).apply {
                        putExtra(DetailTvActivity.EXTRA_ID, it)
                    }
                    startActivity(intent)
                }
            }
            binding.rvTvSeries.apply {
                this.adapter = listTvAdapter
                layoutManager = GridLayoutManager(requireActivity(), 2)
            }
            binding.layoutError.btnTryAgain.isVisible = false
        }
    }

    override fun setSubscribeToLiveData() {
        genreViewModel.tvSeries.observe(this) {
            getLaunch {
                listTvAdapter.submitData(it)
            }
        }

        getLaunch {
            listTvAdapter.loadStateFlow.collectLatest {
                with(binding) {
                    progressBar.isVisible = it.refresh is LoadState.Loading
                    progressBarLoadMore.isVisible = it.append is LoadState.Loading
                    rvTvSeries.isVisible =
                        it.refresh !is LoadState.Loading && it.refresh !is LoadState.Error
                    layoutError.root.isVisible = it.refresh is LoadState.Error

                    val errorState = it.refresh as? LoadState.Error ?: it.append as? LoadState.Error
                    val errorMessage = errorState?.error?.message
                    layoutError.tvInfo.text = errorMessage
                }
            }
        }
    }

}
