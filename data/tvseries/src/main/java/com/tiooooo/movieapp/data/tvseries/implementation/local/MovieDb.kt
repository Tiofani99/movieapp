package com.tiooooo.movieapp.data.tvseries.implementation.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.tiooooo.movieapp.data.tvseries.implementation.local.dao.SearchTvHistoryDao
import com.tiooooo.movieapp.data.tvseries.implementation.local.dao.TvFavoriteDao
import com.tiooooo.movieapp.data.tvseries.implementation.local.entity.SearchTvHistoryEntity
import com.tiooooo.movieapp.data.tvseries.implementation.local.entity.TvFavoriteEntity

@Database(
    entities = [
        SearchTvHistoryEntity::class,
        TvFavoriteEntity::class,
    ],
    version = 2,
)
abstract class TvSeriesDb : RoomDatabase() {
    abstract fun searchHistoryDao(): SearchTvHistoryDao
    abstract fun tvFavoriteDao(): TvFavoriteDao

    companion object {
        private const val DB_NAME = "TvSeries.db"

        @Volatile
        private var INSTANCE: TvSeriesDb? = null

        @Synchronized
        fun getInstance(ctx: Context): TvSeriesDb {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(
                    ctx.applicationContext,
                    TvSeriesDb::class.java,
                    DB_NAME
                ).fallbackToDestructiveMigration().build()
            }
            return INSTANCE as TvSeriesDb
        }
    }
}
