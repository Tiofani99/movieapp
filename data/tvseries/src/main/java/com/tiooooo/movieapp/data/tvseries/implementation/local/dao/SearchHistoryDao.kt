package com.tiooooo.movieapp.data.tvseries.implementation.local.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.tiooooo.movieapp.data.tvseries.implementation.local.entity.SearchTvHistoryEntity

@Dao
interface SearchTvHistoryDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(searchHistory: SearchTvHistoryEntity)

    @Update
    fun update(searchHistory: SearchTvHistoryEntity)

    @Delete
    fun delete(searchHistory: SearchTvHistoryEntity)

    @Query("SELECT * from SearchTvHistoryEntity ORDER BY lastUpdated DESC")
    fun getSearchHistory(): LiveData<List<SearchTvHistoryEntity>>
}
