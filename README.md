![image](/uploads/d7101fbd1812273b24ab73b10deb92ed/image.png)

# MovieApp
[MovieApp](https://drive.google.com/file/d/13Kg01EYrfWxUy32vFjW-jml5j-8FaDGB/view?usp=sharing) is an Android application that allows you to view information about updates movie.


### Overview
This application is developed using various technologies and libraries, including Paging3, Room, Retrofit, Lottie, Shimmer and Dagger Hilt.

### Source API
The data for this application is sourced from a [TMDB API](https://developer.themoviedb.org/docs/getting-started) 

### Key Features
- Display a list of Movies & Tv Series.
- Display favorite movie or tv series
- Movie details including information such as cast, genre, review, and overview.
- Paging capability to handle a large number of movies.
- Integration with the server using Retrofit.
- Dependency and injection with Dagger Hilt.

### Core Technologies
- [Paging3](https://developer.android.com/topic/libraries/architecture/paging/v3-overview) for data pagination.
- [Room](https://developer.android.com/jetpack/androidx/releases/room) for local data storage.
- [Retrofit](https://square.github.io/retrofit/) for server communication.
- [Dagger Hilt](https://dagger.dev/hilt/) for dependency injection.
- [Lotties](https://lottiefiles.com/) for animation.
- [Tourbine](https://github.com/cashapp/turbine) for unit testing.
- [Chucker](https://github.com/ChuckerTeam/chucker) for see traffic log data.

### How to Use
1. Clone this repository to your device.
2. Open the project using Android Studio.
3. Add your `API_KEY` to `build.gradle(:core)`
4. Run the application on an emulator or physical device.


   
### Contribution
We welcome contributions from the developer community. If you would like to contribute to this project, please open a new issue or submit a pull request.

### Screenshots
![image](/uploads/fd148d6ca9da24529b511123c1ef3d14/image.png)
![image](/uploads/b923bd8389ff4076ca4feddb672273e4/image.png)
![image](/uploads/cce87e2a93a66534e90fbe853cf1615e/image.png)
![image](/uploads/03a81e363edb75654366f2f17dee927a/image.png)
![image](/uploads/e4d7e4324ffdc7a1ec34f5ab513fc1e1/image.png)
![image](/uploads/d3cfee007401b631eedaf1d2da800f7a/image.png)
<br>
<br>
Enjoy using the MovieApp! If you have any questions or feedback, please don't hesitate to contact us.
