package com.tiooooo.movieapp.feature.tvseries.pages.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tiooooo.core.network.data.States
import com.tiooooo.movieapp.data.tvseries.api.model.casts.CastTv
import com.tiooooo.movieapp.data.tvseries.api.model.detail.TvDetail
import com.tiooooo.movieapp.data.tvseries.api.model.review.TvReview
import com.tiooooo.movieapp.data.tvseries.api.model.video.TvVideo
import com.tiooooo.movieapp.data.tvseries.api.repository.TvRepository
import com.tiooooo.movieapp.data.tvseries.implementation.local.entity.TvFavoriteEntity
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailTvViewModel @Inject constructor(
    private val tvRepository: TvRepository,
) : ViewModel() {

    private val _tvSeries = MutableSharedFlow<States<TvDetail>>()
    val tvSeries = _tvSeries.asSharedFlow()

    private val _cast = MutableSharedFlow<States<List<CastTv>>>()
    val cast = _cast.asSharedFlow()

    private val _review = MutableSharedFlow<States<List<TvReview>>>()
    val review = _review.asSharedFlow()

    private val _videos = MutableSharedFlow<States<List<TvVideo>>>()
    val videos = _videos.asSharedFlow()

    fun getTvDetail(tvId: String) {
        viewModelScope.launch {
            _tvSeries.emitAll(tvRepository.getDetailTv(tvId))
        }
    }

    fun getTvCasts(tvId: String) {
        viewModelScope.launch {
            _cast.emitAll(tvRepository.getTvCasts(tvId))
        }
    }

    fun getTvReviews(tvId: String) {
        viewModelScope.launch {
            _review.emitAll(tvRepository.getTvReviews(tvId))
        }
    }

    fun getTvVideos(tvId: String) {
        viewModelScope.launch {
            _videos.emitAll(tvRepository.getTvVideos(tvId))
        }
    }

    fun getTvFavoriteById(tvId: Int): LiveData<TvFavoriteEntity> =
        tvRepository.getTvFavoriteById(tvId)

    fun setActionFavorite(
        isFavorite: Boolean,
        tvFavoriteEntity: TvFavoriteEntity,
    ) {
        viewModelScope.launch {
            if (isFavorite) {
                tvRepository.insertTvFavorite(tvFavoriteEntity)
            } else {
                tvRepository.deleteTvFavorite(tvFavoriteEntity)
            }
        }
    }
}
