package com.tiooooo.movieapp.feature.tvseries.pages.main.listener

interface PosterListener {
    fun onClickSeeAll(title: String, type: String)
    fun onClickDetail(tvId: Long)
}
