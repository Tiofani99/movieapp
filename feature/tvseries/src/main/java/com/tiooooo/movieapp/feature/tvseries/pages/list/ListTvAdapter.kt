package com.tiooooo.movieapp.feature.tvseries.pages.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.tiooooo.core.R
import com.tiooooo.core.constant.Constant
import com.tiooooo.core.databinding.ItemPosterFullBinding
import com.tiooooo.movieapp.data.tvseries.api.model.list.TvResult

class ListTvAdapter : PagingDataAdapter<TvResult, ListTvAdapter.ViewHolder>(Companion) {
    companion object : DiffUtil.ItemCallback<TvResult>() {
        override fun areItemsTheSame(oldItem: TvResult, newItem: TvResult): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: TvResult, newItem: TvResult): Boolean {
            return oldItem == newItem
        }
    }

    var onItemClick: ((tvId: String) -> Unit)? = null

    override
    fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bindItem(it)
            holder.itemView.setOnClickListener {
                onItemClick?.invoke(getItem(position)?.id.toString())
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemPosterFullBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    class ViewHolder(
        private val binding: ItemPosterFullBinding,
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bindItem(tvResult: TvResult) {
            binding.apply {
                ivPoster.load(Constant.BASE_IMAGE_500 + tvResult.posterPath) {
                    placeholder(R.drawable.ic_image)
                    error(R.drawable.ic_image)
                    crossfade(true)
                }
                tvTitle.text = tvResult.title
                tvRating.text = tvResult.voteAverage.toString()
            }
        }
    }
}
