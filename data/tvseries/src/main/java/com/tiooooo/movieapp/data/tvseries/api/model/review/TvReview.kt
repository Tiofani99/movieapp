package com.tiooooo.movieapp.data.tvseries.api.model.review

data class TvReview(
    val author: String,
    val authorAva: String,
    val content: String,
    val createdAt: String,
    val id: String,
    val updatedAt: String,
    val url: String,
)

