package com.tiooooo.movieapp.feature.movie.pages.main.listener

interface PosterListener {
    fun onClickSeeAll(title: String, type: String)
    fun onClickDetail(movieId: Long)
}
