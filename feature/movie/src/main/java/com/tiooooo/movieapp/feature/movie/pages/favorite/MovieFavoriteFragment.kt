package com.tiooooo.movieapp.feature.movie.pages.favorite

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.tiooooo.core.base.BaseFragment
import com.tiooooo.core.extensions.gone
import com.tiooooo.movieapp.feature.movie.R
import com.tiooooo.movieapp.feature.movie.databinding.FragmentMovieFavoriteBinding
import com.tiooooo.movieapp.feature.movie.pages.detail.DetailMovieActivity
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class MovieFavoriteFragment : BaseFragment<FragmentMovieFavoriteBinding>(R.layout.fragment_movie_favorite) {
    private val movieFavoriteViewModel: MovieFavoriteViewModel by viewModels()
    private val movieFavoriteAdapter = MovieFavoriteAdapter()

    override fun initView() {
        binding.apply {
            rvMovie.adapter = movieFavoriteAdapter
            rvMovie.layoutManager = GridLayoutManager(requireContext(), 2)
        }
    }

    override fun initListener() {
        binding.swipeRefresh.setOnRefreshListener {
            binding.swipeRefresh.isRefreshing = false
        }

        movieFavoriteAdapter.apply {
            onItemClick = {
                val intent = Intent(requireContext(), DetailMovieActivity::class.java).apply {
                    putExtra(DetailMovieActivity.EXTRA_ID, it)
                }
                startActivity(intent)
            }
        }
    }

    override fun setSubscribeToLiveData() {
        movieFavoriteViewModel.getMovieFavorite().observe(viewLifecycleOwner) {
            movieFavoriteAdapter.setData(it)
            binding.layoutError.apply {
                root.isVisible = it.isEmpty()
                btnTryAgain.isVisible = false
            }
            binding.layoutError.tvInfo.text = "You do not have a favorite yet"
            binding.rvMovie.isVisible = it.isNotEmpty()
        }
    }
}
