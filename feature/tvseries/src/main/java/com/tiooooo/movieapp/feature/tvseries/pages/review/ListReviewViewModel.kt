package com.tiooooo.movieapp.feature.tvseries.pages.review

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.tiooooo.movieapp.data.tvseries.api.model.review.TvReview
import com.tiooooo.movieapp.data.tvseries.api.repository.TvRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ListReviewViewModel @Inject constructor(
    private val tvRepository: TvRepository,
) : ViewModel() {

    private val _reviews = MutableLiveData<PagingData<TvReview>>()
    val reviews: LiveData<PagingData<TvReview>> get() = _reviews

    fun getAllTvReviews(
        movieId: String,
    ) = viewModelScope.launch {
        tvRepository.getAllTvReviews(movieId).cachedIn(viewModelScope).collectLatest {
            _reviews.value = it
        }
    }
}
