package com.tiooooo.movieapp.feature.tvseries.pages.main.adapter.poster

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter
import coil.load
import com.tiooooo.core.R
import com.tiooooo.core.constant.Constant
import com.tiooooo.core.databinding.ItemPosterBinding
import com.tiooooo.movieapp.data.tvseries.api.model.list.TvResult
import com.tiooooo.movieapp.feature.tvseries.pages.main.listener.PosterListener

class PosterAdapter(
    private val handlePosterListener: PosterListener,
) : Adapter<PosterAdapter.ViewHolder>() {
    private val list: MutableList<TvResult> = mutableListOf()

    @SuppressLint("NotifyDataSetChanged")
    fun setData(list: List<TvResult>) {
        this.list.clear()
        this.list.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemPosterBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(list[position])
        holder.itemView.setOnClickListener {
            handlePosterListener.onClickDetail(list[position].id)
        }
    }

    class ViewHolder(
        private val binding: ItemPosterBinding,
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bindItem(tvResult: TvResult) {
            binding.apply {
                ivPoster.load(Constant.BASE_IMAGE_500 + tvResult.posterPath) {
                    placeholder(R.drawable.ic_image)
                    error(R.drawable.ic_image)
                    crossfade(true)
                }
                tvTitle.text = tvResult.title
                tvRating.text = tvResult.voteAverage.toString()
            }
        }
    }
}
