package com.tiooooo.movieapp.data.tvseries.implementation.datasource

import com.tiooooo.core.base.BasePagingSource
import com.tiooooo.movieapp.data.tvseries.implementation.remote.api.TvApi
import com.tiooooo.movieapp.data.tvseries.api.model.review.TvReview
import com.tiooooo.movieapp.data.tvseries.implementation.remote.response.review.toTvReview


class TvReviewPagingSource(
    private val tvApi: TvApi,
    private val movieId: String,
) : BasePagingSource<Int, TvReview>(
    fetch = { position ->
        val res = tvApi.getMovieReviews(movieId, position)
        res.data?.map { it.toTvReview() } ?: emptyList()
    }
)
