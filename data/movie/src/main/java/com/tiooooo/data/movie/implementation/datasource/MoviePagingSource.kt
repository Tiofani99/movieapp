package com.tiooooo.data.movie.implementation.datasource

import com.tiooooo.core.base.BasePagingSource
import com.tiooooo.data.movie.api.model.list.MovieResult
import com.tiooooo.data.movie.implementation.remote.api.MovieApi
import com.tiooooo.data.movie.implementation.remote.response.list.mapToMovieResult

class MoviePagingSource(
    private val movieApi: MovieApi,
    private val type: String,
) : BasePagingSource<Int, MovieResult>(
    fetch = { position ->
        val res = movieApi.getMovies(type, position)
        res.data?.map { it.mapToMovieResult() } ?: emptyList()
    }
)
