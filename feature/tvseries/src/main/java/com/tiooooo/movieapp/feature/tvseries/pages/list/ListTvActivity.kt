package com.tiooooo.movieapp.feature.tvseries.pages.list

import android.content.Intent
import androidx.activity.viewModels
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import com.tiooooo.core.base.BaseActivity
import com.tiooooo.core.extensions.getLaunch
import com.tiooooo.movieapp.feature.tvseries.databinding.ActivityListTvBinding
import com.tiooooo.movieapp.feature.tvseries.pages.detail.DetailTvActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest

@AndroidEntryPoint
class ListTvActivity : BaseActivity<ActivityListTvBinding>() {
    companion object {
        const val EXTRA_TITLE = "EXTRA_TITLE title"
        const val EXTRA_TYPE = "EXTRA_TYPE"
        const val EXTRA_QUERY = "EXTRA_QUERY"
    }
    override fun getViewBinding() = ActivityListTvBinding.inflate(layoutInflater)

    private val listTvViewModel: ListTvViewModel by viewModels()
    private lateinit var listTvAdapter: ListTvAdapter
    private lateinit var title: String
    private lateinit var type: String
    private var query: String = ""

    override fun initView() {
        title = intent.getStringExtra(EXTRA_TITLE) ?: "Popular"
        type = intent.getStringExtra(EXTRA_TYPE) ?: ""
        query = intent.getStringExtra(EXTRA_QUERY) ?: ""

        binding.toolbar.title = title
        setupToolbar(binding.toolbar)
        if (type.isNotEmpty()) {
            listTvViewModel.getAllTvByType(type)
        } else {
            listTvViewModel.getTvByQuery(query)
        }

        listTvAdapter = ListTvAdapter().apply {
            onItemClick = {
                val intent = Intent(this@ListTvActivity, DetailTvActivity::class.java).apply {
                    putExtra(DetailTvActivity.EXTRA_ID, it)
                }
                startActivity(intent)
            }
        }
        binding.rvTvSeries.apply {
            this.adapter = listTvAdapter
            layoutManager = GridLayoutManager(this@ListTvActivity, 2)
        }
        binding.layoutError.btnTryAgain.isVisible = false
    }

    override fun initListener() {
        binding.apply {
            swipeRefresh.setOnRefreshListener {
                if (type.isNotEmpty()) {
                    listTvViewModel.getAllTvByType(type)
                } else {
                    listTvViewModel.getTvByQuery(query)
                }
                swipeRefresh.isRefreshing = false
            }
        }
    }

    override fun setViewModelObservable() {
        super.setViewModelObservable()
        listTvViewModel.tvSeries.observe(this) {
            getLaunch {
                listTvAdapter.submitData(it)
            }
        }

        getLaunch {
            listTvAdapter.loadStateFlow.collectLatest {
                with(binding) {
                    progressBar.isVisible = it.refresh is LoadState.Loading
                    progressBarLoadMore.isVisible = it.append is LoadState.Loading
                    rvTvSeries.isVisible =
                        it.refresh !is LoadState.Loading && it.refresh !is LoadState.Error
                    layoutError.root.isVisible = it.refresh is LoadState.Error

                    val errorState = it.refresh as? LoadState.Error ?: it.append as? LoadState.Error
                    val errorMessage = errorState?.error?.message
                    layoutError.tvInfo.text = errorMessage
                }
            }
        }
    }
}
