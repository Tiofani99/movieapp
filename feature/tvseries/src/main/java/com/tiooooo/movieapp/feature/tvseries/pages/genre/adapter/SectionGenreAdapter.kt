package com.tiooooo.movieapp.feature.tvseries.pages.genre.adapter

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.tiooooo.movieapp.data.tvseries.api.model.list.GenreTv
import com.tiooooo.movieapp.feature.tvseries.pages.genre.GenreTvFragment

class SectionGenreAdapter(
    activity: AppCompatActivity,
    private val listCategory: ArrayList<GenreTv>,
) : FragmentStateAdapter(activity) {

    override fun getItemCount(): Int = listCategory.size

    override fun createFragment(position: Int): Fragment {
        return GenreTvFragment.newInstance(listCategory, position)
    }

}
